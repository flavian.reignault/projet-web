<?php
    include "FonctionsTest.php";

    date_default_timezone_set('Europe/Paris');

    $file_db = new PDO('sqlite:projet.sqlite3');
    $file_db -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

    $file_db -> exec("DROP TABLE questions");
    $file_db -> exec("DROP TABLE reponses");
    $file_db -> exec("DROP TABLE users");

    $file_db -> exec("CREATE TABLE IF NOT EXISTS questions (
        nomQuestion TEXT PRIMARY KEY,
        typeQuestion TEXT,
        question TEXT)");
        
    $file_db -> exec("CREATE TABLE IF NOT EXISTS reponses (
        nomQuestion TEXT,
        reponse TEXT,
        vrai INT,
        score INTEGER,
        PRIMARY KEY (nomQuestion, reponse),
        FOREIGN KEY (nomQuestion) REFERENCES questions(nomQuestion))");

    $file_db -> exec("CREATE TABLE IF NOT EXISTS users (
        roleUser TEXT,
        loginUser TEXT,
        mdpUser TEXT,
        PRIMARY KEY (loginUser, mdpUser))");


    $insert_questions = "INSERT INTO questions (nomQuestion, typeQuestion, question) VALUES (:nomQuestion, :typeQuestion, :question)";
    $stmt_questions = $file_db -> prepare($insert_questions);
    $stmt_questions -> bindParam(':nomQuestion', $nomQuestion);
    $stmt_questions -> bindParam(':typeQuestion', $typeQuestion);
    $stmt_questions -> bindParam(':question', $question);

    foreach ($questions_sport as $q) {
        $nomQuestion = $q['name'];
        $typeQuestion = $q['type'];
        $question = $q['text'];
        $stmt_questions -> execute();
    }
    foreach ($questions_ciné as $q) {
        $nomQuestion = $q['name'];
        $typeQuestion = $q['type'];
        $question = $q['text'];
        $stmt_questions -> execute();
    }
    foreach ($questions_musique as $q) {
        $nomQuestion = $q['name'];
        $typeQuestion = $q['type'];
        $question = $q['text'];
        $stmt_questions -> execute();
    }

    $insert_reponses = "INSERT INTO reponses (nomQuestion, reponse, vrai, score) VALUES (:nomQuestion, :reponse, :vrai, :score)";
    $stmt_reponses = $file_db -> prepare($insert_reponses);
    $stmt_reponses -> bindParam(':nomQuestion', $nomQuestion);
    $stmt_reponses -> bindParam(':reponse', $reponse);
    $stmt_reponses -> bindParam(':vrai', $vrai);
    $stmt_reponses -> bindParam(':score', $score);
    foreach ($questions_sport as $q) {
        if (array_key_exists('choices', $q)) {
            foreach ($q['choices'] as $c) {
                $nomQuestion = $q['name'];
                $reponse = $c['text'];
                if (in_array($c['value'], (array)$q['answer'])) {
                    $vrai = 1;
                }
                else {
                    $vrai = 0;
                }
                $score = $q['score'];
                $stmt_reponses -> execute();
            }
        }
        else {
            $nomQuestion = $q['name'];
            $reponse = $q['answer'];
            $vrai = 1;
            $score = $q['score'];
            $stmt_reponses -> execute();
        }
    }
    foreach ($questions_ciné as $q) {
        if (array_key_exists('choices', $q)) {
            foreach ($q['choices'] as $c) {
                $nomQuestion = $q['name'];
                $reponse = $c['text'];
                if (in_array($c['value'], (array)$q['answer'])) {
                    $vrai = 1;
                }
                else {
                    $vrai = 0;
                }
                $score = $q['score'];
                $stmt_reponses -> execute();
            }
        }
        else {
            $nomQuestion = $q['name'];
            $reponse = $q['answer'];
            $vrai = 1;
            $score = $q['score'];
            $stmt_reponses -> execute();
        }
    }
    foreach ($questions_musique as $q) {
        if (array_key_exists('choices', $q)) {
            foreach ($q['choices'] as $c) {
                $nomQuestion = $q['name'];
                $reponse = $c['text'];
                if (in_array($c['value'], (array)$q['answer'])) {
                    $vrai = 1;
                }
                else {
                    $vrai = 0;
                }
                $score = $q['score'];
                $stmt_reponses -> execute();
            }
        }
        else {
            $nomQuestion = $q['name'];
            $reponse = $q['answer'];
            $vrai = 1;
            $score = $q['score'];
            $stmt_reponses -> execute();
        }
    }

    $insert_users = "INSERT INTO users (roleUser, loginUser, mdpUser) VALUES (:roleUser, :loginUser, :mdpUser)";
    $stmt_users = $file_db -> prepare($insert_users);
    $stmt_users -> bindParam(':roleUser', $roleUser);
    $stmt_users -> bindParam(':loginUser', $loginUser);
    $stmt_users -> bindParam(':mdpUser', $mdpUser);

    $roleUser = 'admin';
    $loginUser = 'flav';
    $mdpUser = 'flav';
    $stmt_users -> execute();

    $roleUser = 'admin';
    $loginUser = 'alex';
    $mdpUser = 'alex';
    $stmt_users -> execute();
    
    $roleUser = 'user';
    $loginUser = 'test';
    $mdpUser = 'test';
    $stmt_users -> execute();

    $file_db = null;
?>