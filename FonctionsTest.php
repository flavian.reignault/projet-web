<?php
    include "VariablesTest.php";

    function question_unique($q) {
        if ($q['type'] == 'color') {
            echo "<label class=quest>" . $q['text'] . "</label><br/><input type='$q[type]' value=#C28F19 name='$q[name]' required><br/><br/>";
        }
        else {
            echo "<label class=quest>" . $q['text'] . "</label><br/><input type='$q[type]' name='$q[name]' required><br/><br/>";
        }
    }

    function question_radio($q) {
        $html = "<label class=quest>" . $q['text'] . "</label><br/>";
        $i = 0;
        foreach ($q['choices'] as $c) {
            $i += 1;
            $html .= "<input type='$q[type]' name='$q[name]' value='$c[value]' id='$q[name]-$i' required>";
            $html .= "<label for='$q[name]-$i'>$c[text]</label>";
            $html .= "<br/>";
        }
        echo $html . "<br/>";
    }

    function question_checkbox($q) {
        $html = "<label class=quest>" . $q['text'] . "</label><br/>";
        $i = 0;
        foreach ($q['choices'] as $c) {
            $i += 1;
            $html .= "<input type='$q[type]' name='$q[name]' value='$c[value]' id='$q[name]-$i'>";
            $html .= "<label for='$q[name]-$i'>$c[text]</label>";
            $html .= "<br/>";
        }
        echo $html . "<br/>";
    }

    function question_select($q){
        $html = "<label class=quest>" . $q['text'] . "</label><br/>";
        $html .= "<select name=$q[name]>";
        $i = 0;
        foreach ($q['choices'] as $c) {
            $i += 1;
            $html .= "<option value=$c[value]>$c[text]</option>";
            $html .= "<br/>";
        }
        $html .= "</select>";
        echo $html . "<br/><br/>";
    }


    function answer_simple($q) {
        global $score_max, $score_question, $score;

        $file_db = new PDO('sqlite:projet.sqlite3');
        $result = $file_db -> query("select * from questions natural join reponses where nomQuestion='".$q['name']."' and reponse='".$_POST[$q['name']]."'");
        
        foreach ($result as $r) {
            $score_max += $r['score'];

            echo "<label class=quest>" . $r['question'] . "</label><br/>";

            if ($r['reponse'] == $q['answer']) {
                echo "<label class=vert>Bonne réponse</label><br/>";
                $score_question = $r['score'];
                $score += $r['score'];
            }
            echo "Note : " . $score_question . "/" . $r['score'] . "<br/><br/>";
            $score_question = 0;
        }

        if (!($_POST[$q['name']] == $q['answer'])) {
            $score_max += $q['score'];
            echo "<label class=quest>" . $q['text'] . "</label><br/>";
            echo "<label class=rouge>Mauvaise réponse</label><br/>";
            $score_question = 0;

            echo "Note : " . $score_question . "/" . $q['score'] . "<br/><br/>";
        }
        
        $file_db = null;
    }

    function answer_radio_select($q) {
        global $score_max, $score_question, $score;

        $file_db = new PDO('sqlite:projet.sqlite3');
        $result = $file_db -> query("select * from questions natural join reponses where nomQuestion='".$q['name']."' and reponse='".$_POST[$q['name']]."'");
        
        foreach ($result as $r) {
            $score_max += $r['score'];

            echo "<label class=quest>" . $r['question'] . "</label><br/>";

            if ($r['reponse'] == $q['answer']) {
                echo "<label class=vert>Bonne réponse</label><br/>";
                $score_question = $r['score'];
                $score += $r['score'];
            }
            else { echo "<label class=rouge>Mauvaise réponse</label><br/>"; }
            
            echo "Note : " . $score_question . "/" . $r['score'] . "<br/><br/>";
            $score_question = 0;
            break;
        }
        
        $file_db = null;
    }

    function answer_checkbox($q) {
        global $score_max, $score_question, $score;

        $file_db = new PDO('sqlite:projet.sqlite3');
        $result = $file_db -> query("select * from questions natural join reponses where nomQuestion='".$q['name']."' and vrai=1");
         
        foreach ($result as $r) {
            $score_max += $r['score'];

            $q['name'] = substr($q['name'], 0, -2);

            echo "<label class=quest>" . $r['question'] . "</label><br/>";

            if ($_POST[$q['name']] == $q['answer']) {
                echo "<label class=vert>Bonne réponse</label><br/>";
                $score_question = $r['score'];
                $score += $r['score'];
            }
            else { echo "<label class=rouge>Mauvaise réponse</label><br/>"; }
    
            echo "Note : " . $score_question . "/" . $r['score'] . "<br/><br/>";
            $score_question = 0;
            break;
        }
        $file_db = null;
    }

    function affiche_connexion($c) {
        $html = "<label class=quest>" . $c['text'] . "</label><br/>";
        $html .= "<input class=label type=". $c['type'] . " name=" . $c['name'] . " placeholder=" . $c['placeholder'] . " required /><br/>";
        echo $html;
    }

    function affiche_role($c) {
        $html = "<div class=select>";
        $html .= "<label class=quest>" . $c['text'] . "</label>";
        $html .= "<select class=role name=$c[name]>";
        $i = 0;
        foreach ($c['choices'] as $x) {
            $i += 1;
            $html .= "<option value=$x[value]>$x[text]</option>";
            $html .= "<br/>";
        }
        $html .= "</select></div>";
        echo $html . "<br/><br/>";
    }
?>