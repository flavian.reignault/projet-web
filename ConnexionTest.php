<?php
    include "FonctionsTest.php";
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="Connexion.css">
        <title>Test de Culture G</title>
    </head>

    <body>

        <form action="verifConnexion.php" method="POST">

            <fieldset>
                <?php
                    if (isset($_POST['submit']) and isset($test)){
                        echo "<label class=erreur>ATTENTION : Rôle, Indentifiant ou Mot de passe incorrect !</label><br/><br/>";
                    }
                ?>
                <legend><h1>Connectez-vous</h1></legend>
                    <?php
                        foreach ($connexion_role as $cr) {
                            $connexion_role_handlers[$cr['type']]($cr);
                        }
                        foreach ($connexion as $c) {
                            $connexion_handlers[$c['type']]($c);
                        }
                    ?>

                    <input id="submit" type="submit" name="submit" value="Se connecter" />
            </fieldset>
        </form>
        <a href="CreationCompte.php"><input class="create" type="submit" name="submit" value="Créer un compte" /></a>
    </body>
</html>