<?php
    $questions_sport = [
        array(
            "name" => "basket1",
            "type" => "text",
            "text" => "Qui est l'inventeur du basket-ball ?",
            "answer" => "James Naismith",
            "score" => 2,
        ),
        array(
            "name" => "decathlon1",
            "type" => "radio",
            "text" => "Combien y'a t-il d'épreuves dans un décathlon ?",
            "choices" => [
                array(
                    "text" => "5",
                    "value" => "5",
                ),
                array(
                    "text" => "10",
                    "value" => "10",
                ),
                array(
                    "text" => "3",
                    "value" => "3",
                ),
                array(
                    "text" => "12",
                    "value" => "12",
                ),
            ],
            "answer" => "10",
            "score" => 1,
        ),
        array(
            "name" => "tennis",
            "type" => "select",
            "text" => "Combien de Roland Garros a gagné Rafael Nadal ?",
            "choices" => [
                array(
                    "text" => "Choisissez une proposition",
                    "value" => "Choisissez une proposition",
                ),
                array(
                    "text" => "11",
                    "value" => "11",
                ),
                array(
                    "text" => "13",
                    "value" => "13",
                ),
                array(
                    "text" => "14",
                    "value" => "14",
                ),
                array(
                    "text" => "12",
                    "value" => "12",
                ),
            ],
            "answer" => "13",
            "score" => 1.5,
        ),
        array(
            "name" => "foot1[]",
            "type" => "checkbox",
            "text" => "Cochez tous les joueurs ayant gagné la Coupe du Monde 98'",
            "choices" => [
                array(
                    "text" => "Zinedine Zidane",
                    "value" => "Zinedine Zidane",
                ),
                array(
                    "text" => "Didier Deschamps",
                    "value" => "Didier Deschamps",
                ),
                array(
                    "text" => "Alain Boghossian",
                    "value" => "Alain Boghossian",
                ),
                array(
                    "text" => "Michel Platini",
                    "value" => "Michel Platini",
                ),
                array(
                    "text" => "Marius Trésor",
                    "value" => "Marius Trésor",
                ),
                array(
                    "text" => "Vincent Candela",
                    "value" => "Vincent Candela",
                ),                
                array(
                    "text" => "Christian Karembeu",
                    "value" => "Christian Karembeu",
                ),                
                array(
                    "text" => "Bixente Lizarazu",
                    "value" => "Bixente Lizarazu",
                ),                
                array(
                    "text" => "Lionel Charbonnier",
                    "value" => "Lionel Charbonnier",
                ),                
                array(
                    "text" => "Florent Malouda",
                    "value" => "Florent Malouda",
                ),
                array(
                    "text" => "Franck Ribéry",
                    "value" => "Franck Ribéry",
                ),                
                array(
                    "text" => "Marius Thuram",
                    "value" => "Marius Thuram",
                ),                
                array(
                    "text" => "Raymond Kopa",
                    "value" => "Raymond Kopa",
                ),                
                array(
                    "text" => "Just Fontaine",
                    "value" => "Just Fontaine",
                )
            ],
            "answer" => array(
                "Zinedine Zidane",
                "Didier Deschamps",
                "Alain Boghossian",
                "Vincent Candela",
                "Christian Karembeu",
                "Bixente Lizarazu",
                "Lionel Charbonnier"
            ),
            "score" => 1.5,
        ),
        // array(
        //     "name" => "inconnus",
        //     "type" => "radio",
        //     "text" => "Parmi ces sports, lequel n'existe pas ?",
        //     "choices" => [
        //         array(
        //             "text" => "Le foot avec des tazers (Ultimate tazer ball)",
        //             "value" => "Le foot avec des tazers(Ultimate tazer ball)"
        //         ),
        //         array(
        //             "text" => "La course de lapins (KaninShop)",
        //             "value" => "La course de lapins (KaninShop)"
        //         ),                
        //         array(
        //             "text" => "Le hockey sous l'eau (Octopush)",
        //             "value" => "Le hockey sous l'eau (Octopush)"
        //         ),
        //         array(
        //             "text" => "Le lancé d'Iphone (AppleThrow)",
        //             "value" => "LelancédIphone"
        //         ),
        //         array(
        //             "text" => "La boxe échecs (ChessBoxing)",
        //             "value" => "La boxe échecs (ChessBoxing)"
        //         )
        //     ],
        //     "answer" => "LelancédIphone",
        //     "score" => 1.5,
        // )
    ];

    $questions_ciné = [
        array(
            "name" => "episodes",
            "type" => "radio",
            "text" => "Combien d'épisodes comprend la série 'Les feux de l'amour' ?",
            "choices" => [
                array(
                    "text" => "11 203",
                    "value" => "11 203",
                ),
                array(
                    "text" => "12 203",
                    "value" => "12 203",
                ),
                array(
                    "text" => "13 203",
                    "value" => "13 203",
                ),
                array(
                    "text" => "14 203",
                    "value" => "14 203",
                ),
            ],
            "answer" => "12 203",
            "score" => 2,
        ),
        array(
            "name" => "dinsey1",
            "type" => "number",
            "text" => "En quelle année est sorti le premier long métrage Disney ?",
            "answer" => 1938,
            "score" => 2,
        ),
        // array(
        //     "name" => "disney2",
        //     "type" => "select",
        //     "text" => "Quel est le premier long métrage Disney ?",
        //     "choices" => [
        //         array(
        //             "text" => "Choisissez une proposition",
        //             "value" => "Choisissez une proposition",
        //         ),
        //         array(
        //             "text" => "La Belle au Bois dormant",
        //             "value" => "La Belle au Bois dormant",
        //         ),
        //         array(
        //             "text" => "Pinocchio",
        //             "value" => "Pinocchio",
        //         ),
        //         array(
        //             "text" => "Blanche Neige et les 7 nains",
        //             "value" => "BlancheNeigeetles7nains",
        //         ),
        //         array(
        //             "text" => "Bambi",
        //             "value" => "Bambi",
        //         ),
        //         array(
        //             "text" => "The Mask",
        //             "value" => "The Mask",
        //         ),
        //         array(
        //             "text" => "Cette compagnie n'a jamais existé !",
        //             "value" => "Cette compagnie n'a jamais existé !",
        //         )
        //     ],
        //     "answer" => "BlancheNeigeetles7nains",
        //     "score" => 1,
        // ),
        array(
            "name" => "DeFunes[]",
            "type" => "checkbox",
            "text" => "Cochez les films dans lesquels a joué Louis de Funes",
            "choices" => [
                array(
                    "text" => "La soupe aux choux",
                    "value" => "La soupe aux choux",
                ),
                array(
                    "text" => "La grande vadrouille",
                    "value" => "La grande vadrouille",
                ),
                array(
                    "text" => "Le mur de l'Atlantique",
                    "value" => "Le mur de l'Atlantique",
                ),
                array(
                    "text" => "Hibernatus",
                    "value" => "Hibernatus",
                ),
                array(
                    "text" => "La 7e compagnie",
                    "value" => "La 7e compagnie",
                ),
                array(
                    "text" => "Casino Royale",
                    "value" => "Casino Royale",
                ),                
                array(
                    "text" => "Le bon, la brute et le truand",
                    "value" => "Le bon, la brute et le truand",
                ),                
                array(
                    "text" => "Le gendarme de Saint-Tropez",
                    "value" => "Le gendarme de Saint-Tropez",
                ),                
                array(
                    "text" => "Fantomas",
                    "value" => "Fantomas",
                )
            ],
            "answer" => array(
                "La soupe aux choux",
                "La grande vadrouille",
                "Hibernatus",
                "Le gendarme de Saint-Tropez",
                "Fantomas",
            ),
            "score" => 1,
        ),
        array(
            "name" => "oscar1",
            "type" => "radio",
            "text" => "Combien d'Oscars détient le film 'Titanic' ?",
            "choices" => [
                array(
                    "text" => "9",
                    "value" => "9",
                ),
                array(
                    "text" => "11",
                    "value" => "11",
                ),
                array(
                    "text" => "10",
                    "value" => "10",
                ),
                array(
                    "text" => "12",
                    "value" => "12",
                ),
            ],
            "answer" => "11",
            "score" => 2,
        ),
    ];

    $questions_musique = [
        array(
            "name" => "CDM",
            "type" => "radio",
            "text" => "Qui est l'interprète de la chanson de la coupe du monde 98' ?",
            "choices" => [
                array(
                    "text" => "Jul",
                    "value" => "Jul",
                ),
                array(
                    "text" => "Gloria Gaynor",
                    "value" => "Gloria Gaynor",
                ),
                array(
                    "text" => "Ricky Martin",
                    "value" => "Ricky Martin",
                ),
                array(
                    "text" => "Shakira",
                    "value" => "Shakira",
                ),
                array(
                    "text" => "Jennifer Lopez",
                    "value" => "Jennifer Lopez",
                ),
            ],
            "answer" => "Ricky Martin",
            "score" => 1.5,
        ),
        array(
            "name" => "MichaelJackson",
            "type" => "number",
            "text" => "En quelle année est mort Michael Jackson ?",
            "answer" => 2009,
            "score" => 1,
        ),
        // array(
        //     "name" => "epoque",
        //     "type" => "radio",
        //     "text" => "Dans quelle époque musicale Antonio Vivaldi se situait-il ?",
        //     "choices" => [
        //         array(
        //             "text" => "Classique",
        //             "value" => "Classique"
        //         ),
        //         array(
        //             "text" => "Baroque",
        //             "value" => "Baroque"
        //         ),
        //         array(
        //             "text" => "Romantique",
        //             "value" => "Romantique"
        //         ),
        //         array(
        //             "text" => "Renaissance",
        //             "value" => "Renaissance"
        //         ),
        //         array(
        //             "text" => "Moderne",
        //             "value" => "Moderne"
        //         ),
        //         array(
        //             "text" => "Aucune, ce compositeur n'a jamais existé !",
        //             "value" => "Aucune, ce compositeur n'a jamais existé !"
        //         )
        //     ],
        //     "answer" => "Baroque",
        //     "score" => 1
        // ),
        array(
            "name" => "donda",
            "type" => "color",
            "text" => "Quelle est la couleur du dernier album de Kanye West ?",
            "answer" => "#000000",
            "score" => 1
        ),
        array(
            "name" => "chansons[]",
            "type" => "checkbox",
            "text" => "Quelles sont les 3 chansons les plus écoutées de la décénnie ?",
            "choices" => [
                array(
                    "text" => "Rockstar – Post Malone (feat. 21 Savage)",
                    "value" => "Rockstar – Post Malone (feat. 21 Savage)"
                ),
                array(
                    "text" => "Baby Shark",
                    "value" => "Baby Shark"
                ),
                array(
                    "text" => "Shape of You – Ed Sheeran",
                    "value" => "Shape of You – Ed Sheeran"
                ),
                array(
                    "text" => "One Dance – Drake",
                    "value" => "One Dance – Drake"
                ),
                array(
                    "text" => "Despacito - Luis Fonsi (feat. Daddy Yankee)",
                    "value" => "Despacito - Luis Fonsi (feat. Daddy Yankee)"
                ),
                array(
                    "text" => "Gangnam Style - Psy",
                    "value" => "Gangnam Style - Psy"
                )
            ],
            "answer" => array(
                "Rockstar – Post Malone (feat. 21 Savage)",
                "Shape of You – Ed Sheeran",
                "One Dance – Drake",
            ),
            "score" => 1.5
        )

    ];

    $score_max_sport = 0;
    $score_sport = 0;

    $score_max_ciné = 0;
    $score_ciné = 0;

    $score_max_musique = 0;
    $score_musique = 0;

    $score_question = 0;
    $score_max = 0;
    $score = 0;

    $question_handlers = array(
        "text" => "question_unique",
        "number" => "question_unique",
        "radio" => "question_radio",
        "checkbox" => "question_checkbox",
        "select" => "question_select",
        "color" => "question_unique"
    );
    $answer_handlers = array(
        "text" => "answer_simple",
        "number" => "answer_simple",
        "radio" => "answer_radio_select",
        "checkbox" => "answer_checkbox",
        "select" => "answer_radio_select",
        "color" => "answer_simple"
    );

    $connexion = array(
        array(
            "name" => "login",
            "type" => "text",
            "text" => "Identifiant",
            "placeholder" => "Jack",
        ),
        array(
            "name" => "mdp",
            "type" => "password",
            "text" => "Mot de passe",
            "placeholder" => "Daniels"
        )
    );

    $connexion_handlers = array(
        "text" => "affiche_connexion",
        "password" => "affiche_connexion",
    );

    $connexion_role = array(
        array(
            "name" => "role",
            "type" => "select",
            "text" => "Se connecter en tant que : ",
            "choices" => [
                array(
                    "text" => "Utilisateur",
                    "value" => "Utilisateur"
                ),
                array(
                    "text" => "Administrateur",
                    "value" => "Administrateur"
                ),
            ],
        ),
    );

    $connexion_role_handlers = array(
        "select" => "affiche_role"
    )
?>