<?php
    include "FonctionsTest.php";
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="Connexion.css">
        <title>Test de Culture G</title>
    </head>

    <body>

        <form class="quitter" action="ConnexionTest.php" method="POST">
            <input class="deco" type="submit" name="submit" value="Page d'accueil" />
        </form>

        <form action="verifSuppression.php" method="POST">

            <fieldset>
                <?php
                    if (isset($_POST['submit']) and isset($test)){
                        echo "<label class=erreur>ATTENTION : Compte inexistant !</label><br/><br/>";
                    }
                ?>
                <legend><h1>Supprimez un compte</h1></legend>
                    <?php
                        foreach ($connexion as $c) {
                            $connexion_handlers[$c['type']]($c);
                        }
                    ?>
                    <input id="submit" type="submit" name="submit" value="Supprimer le compte" />
            </fieldset>
        </form>
    </body>
</html>