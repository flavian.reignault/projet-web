<?php
    include "FonctionsTest.php";
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="Test.css">
        <title>Test de Culture G</title>
    </head>

    <body>
        <header>
            <h1>Test de Culture G</h1>
        </header>

        <form class="quitter" action="ConnexionTest.php" method="POST">
            <input class="deco" type="submit" name="submit" value="Déconnexion" />
        </form>

        <form action="Test.php" method="POST">
            <fieldset>
                <legend><h2>Sport</h2></legend>
                    <?php
                        foreach ($questions_sport as $q) {
                            $answer_handlers[$q['type']]($q);
                        }
                        echo "<label class=note>Note sur le sport : " . $score . "/" . $score_max . "</label><br/><br/>";
                        $score_sport = $score;
                        $score = 0;
                        $score_max_sport = $score_max;
                        $score_max = 0;
                    ?>
            </fieldset>

            <fieldset>
                <legend><h2>Le grand écran</h2></legend>
                    <?php
                        foreach ($questions_ciné as $q) {
                            $answer_handlers[$q['type']]($q);
                        }
                        echo "<label class=note>Note sur le cinéma : " . $score . "/" . $score_max . "</label><br/><br/>";
                        $score_ciné = $score;
                        $score = 0;
                        $score_max_ciné = $score_max;
                        $score_max = 0;
                    ?>
            </fieldset>

            <fieldset>
                <legend><h2>Le rythme dans la peau</h2></legend>
                    <?php
                        foreach ($questions_musique as $q) {
                            $answer_handlers[$q['type']]($q);
                        }
                        echo "<label class=note>Note sur la musique : " . $score . "/" . $score_max . "</label><br/><br/>";
                        $score_musique = $score;
                        $score = 0;
                        $score_max_musique = $score_max;
                        $score_max = 0;
                    ?>
            </fieldset>

            <fieldset>
                <legend><h2>Résultat</h2></legend>
                        <?php
                            $score = $score_sport + $score_ciné + $score_musique;
                            $score_max = $score_max_sport + $score_max_ciné + $score_max_musique;
                            echo "<label>Votre note finale : " . $score . "/" . $score_max . "</label><br/>";
                            if ($score <= $score_max/3) {
                                echo "<p>Honnêtement, ce n'est pas terrible. Vous avez encore pas mal de choses à revoir !</p>";
                            }
                            elseif ($score_max/3 < $score && $score <= 2*$score_max/3) {
                                echo "<p>Résultat correct mais vous pouvez encore vous améliorer.</p>";
                            }
                            else {
                                echo "<p>Vous avez de très bonnes connaissances. Félicitation !</p>";
                            }
                        ?>
            </fieldset>

            <input class="submit" type="submit" name="submit" value="Refaire le Test" />
        </form>
    </body>
</html>