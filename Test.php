<?php
    include "FonctionsTest.php";
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="Test.css">
        <title>Test de Culture G</title>
    </head>

    <body>
        <header>
            <h1>Test de Culture G</h1>
        </header>

        <?php
            if (isset($pouvoir)){
                echo "<form class=suppr action=SuppressionCompte.php method=POST>";
                echo "<input class=delete type=submit name=submit value='Supprimer un compte' />";
                echo "</form>";
            }
        ?>

        <form class="quitter" action="ConnexionTest.php" method="POST">
            <input class="deco" type="submit" name="submit" value="Déconnexion" />
        </form>
        
        <?php
            if (isset($_POST['submit']) and isset($test)){
                echo "<label class=erreur>ATTENTION : Vous n'avez pas rempli toutes les questions !</label><br/><br/>";
            }
        ?>

        <form action="verifTest.php" method="POST">
            <fieldset>
                    <legend><h2>Avant de commencer</h2></legend>
                        <p>Ce questionnaire comporte des questions sur différents domaines (sport, cinéma, musique)</p>
                        <p>Afin de mener à bien cette épreuve, il vous est demandé de remplir toutes les réponses<br/>(si vous ne savez pas, mettez une réponse au hasard)</p>
                        <p>À vous de jouer !</p>
                </fieldset>

            <fieldset>
                <legend><h2>Sport</h2></legend>
                    <?php
                        foreach ($questions_sport as $q) {
                            $question_handlers[$q['type']]($q);
                        }
                    ?>
            </fieldset>

            <fieldset>
                <legend><h2>Le grand écran</h2></legend>
                    <?php
                        foreach ($questions_ciné as $q) {
                            $question_handlers[$q['type']]($q);
                        }
                    ?>
            </fieldset>

            <fieldset>
                <legend><h2>Le rythme dans la peau</h2></legend>
                    <?php
                        foreach ($questions_musique as $q) {
                            $question_handlers[$q['type']]($q);
                        }
                    ?>
            </fieldset>

            <input class="submit" type="submit" name="submit" value="Terminer le test" />
        </form>
    </body>
</html>